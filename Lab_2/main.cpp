/*
* ������ୠ� ࠡ�� �2
* "��᫥������� �����-�뢮�� �⠭���⭮� ������⥪� �++ � ⨯�� ������ � IDE (��⥣�஢����� �।� ࠧࠡ�⪨) Qt Creator"
*
* Developer: ���㪮� �.�.
*/

#include <iostream> // ������砥� ������⥪� ��� �����/�뢮��
#include <iomanip> // ������砥� ������⥪� ��� �ᯮ�쮢���� ��������஢ �ଠ�஢����


int main() // ������ �᭮���� �㭪��
{

std::cout << "�ணࠬ�� �1\n" << std::endl;


{
    using namespace std;
    cout << "*****\t\t\t\t\t�����\t\t\t\t\t*****" << endl;
    cout.width(85); // ������, ����� ������ �ਭ� ����;
    cout << "�������: ���. ��ࠧ������"<< endl;
    cout.width(85);// ������, ����� ������ �ਭ� ����;
    cout << "��㯯�: 310" << endl;
    cout << right << setw(85) << "��㤥��: ���㪮� ��ࣥ�" << endl; // �ᯮ��㥬 ��� ��⠭���� �ਭ� ���� 85 ��������� setw(n)
    cout << "\t\t\t\t     �.���� 2023" << endl;
    std::cout<< std::endl;
}
#if 0
#endif

std::cout << "�ணࠬ�� �2\n" << std::endl;

    std::cout << "bool:\t\t" << sizeof(bool) << " bytes" << std::endl;
    std::cout << "char:\t\t" << sizeof(char) << " bytes" << std::endl;
    std::cout << "wchar_t:\t" << sizeof(wchar_t) << " bytes" << std::endl;
    std::cout << "char16_t:\t" << sizeof(char16_t) << " bytes" << std::endl;
    std::cout << "char32_t:\t" << sizeof(char32_t) << " bytes" << std::endl;
    std::cout << "short:\t\t" << sizeof(short) << " bytes" << std::endl;
    std::cout << "int:\t\t" << sizeof(int) << " bytes" << std::endl;
    std::cout << "long:\t\t" << sizeof(long) << " bytes" << std::endl;
    std::cout << "long long:\t" << sizeof(long long) << " bytes" << std::endl;
    std::cout << "float:\t\t" << sizeof(float) << " bytes" << std::endl;
    std::cout << "double:\t\t" << sizeof(double) << " bytes" << std::endl;
    std::cout << "long double:\t" << sizeof(long double) << " bytes\n" << std::endl;




std::cout << "�ணࠬ�� �3\n" << std::endl;

    double a, b, c; // ������� ��� ��६�����
    std::cout << "������ ���祭���� � =  ";
    std::cin >> a; // ���� ��६����� � ����������
    std::cout << "������ ���祭���� b =  ";
    std::cin >> b; // ���� ��६����� � ����������
    std::cout << "������ ���祭���� c =  ";
    std::cin >> c; // ���� ��६����� � ����������
    std::cout << "�।��� ��䬥��᪮� ��� ������ ���祭�� =  " << (a+b+c)/3 << std::endl;
    std::cout<< std::endl;



std::cout << "�ணࠬ�� �4\n" << std::endl;
{
    std::cout << "���᫥��� ᪮��� ����筮�� ��ꥪ�:"<< std::endl;
    std::cout << "������ ����ﭥ S(��) =  ";
    double distance;
    std::cin >> distance; // ���� ����ﭨ� � ����������
    std::cout << "������ �६� ��� t(�) =  ";
    double time;
    std::cin >> time; // ���� �६��� � ����������
    double speed = distance/time;
    std::cout << "������� �������� ��⠢��� = "<< speed << " ��/� " << std::endl;
    std::cout<< std::endl;
}
{
    std::cout << "���᫥��� �᪮७��:"<< std::endl;
    std::cout << "������ ��砫��� ᪮���� �������� V1(�) =  ";
    double speed1;
    std::cin >> speed1; // ���� ��砫쭮� ᪮��� ��������
    std::cout << "������ ������� ᪮���� �������� V2(�) =  ";
    double speed2;
    std::cin >> speed2; // ���� ����筮� ᪮��� ��������
    std::cout << "������ �६� �� ���஥ ���������� ᪮���� t(�) =  ";
    double time;
    std::cin >> time; // ���� �६��� � ����������
    double acceleration = (speed2 - speed1) / time;
    std::cout << "�᪮७�� ��⠢��� = "<< acceleration << " �/�2"<< std::endl;
    std::cout<< std::endl;
}
{
    using namespace std;
   cout << "���᫥��� ࠤ��� ��㣠:" << endl;
   double P, PI (3.14);
   cout << "������ ����� ���㦭��� �(��) =  ";
   cin >> P; // ���� ����� ���㦭���
   cout << "������ R =  "<< setprecision(3) << P/(2*PI) << " ��";
   cout<< endl;
}

std::cout << "�ணࠬ�� �5\n" << std::endl;
{
    using namespace std;
    double a1(34.50);//��室��� ���祭��
    double b1(0.004000);//��室��� ���祭��
    double c1(123.005);//��室��� ���祭��
    double d1(146000); //��室��� ���祭��

    std::cout << "��室��� ���祭�� �� ��: " << a1 << std::endl;
    std::cout << "��室��� ���祭�� �� ��:" << b1 << std::endl;
    std::cout << "��室��� ���祭�� �� ��: " << c1 << std::endl;
    std::cout << "��室��� ���祭�� �� ��: " << d1 << std::endl;
    std::cout << "������ ���祭�� � ��ᯮ���樠�쭮� �ଥ: " << scientific << a1 << std::endl;
    std::cout << "������ ���祭�� � ��ᯮ���樠�쭮� �ଥ: " << b1 << std::endl;
    std::cout << "������ ���祭�� � ��ᯮ���樠�쭮� �ଥ: " << c1 << std::endl;
    std::cout << "������ ���祭�� � ��ᯮ���樠�쭮� �ଥ: " << d1 << std::endl;
}
    return 0; // �����頥��� ���祭��

}
