#include <iostream> //������祭�� �����⥪� �����/�뢮��
#include <cmath>//������祭�� �����⥪� ��� �ᯮ�짮���� �㭪樨 pow - ���������� � �⥯���

#if 0
�� �㦭� ��� ⮣�, �⮡� � ��砥 ����室����� ����� �뫮 ����� ᪮��஢��� � �⪫���� ���� �ணࠬ��
#endif


/*������� �3*/
double MyFunction (double x=2); //������� �㭪樨 MyFunction � ��ࠬ��஬ �� 㬮�砭�� = 2
double MyFunction (double x) // ��।������ �㭪樨 MyFunction
{
    return pow(x, 3); //�㭪樨 ���������� � �⥯���
}

int main()
{
    std::cout << "���祭�� �� 㬮�砭�� � ���쥩 �⥯�� �㤥� ࠢ�� =  " << MyFunction () << std::endl;
    std::cout << "������ �� ���祭�� =  ";
    double a;
    std::cin >> a;
    std::cout << "��������� ���� ���祭�� � ���쥩 �⥯�� �㤥� ࠢ�� =  " << MyFunction (a) << std::endl;
    return 0;
}



/*������� �4*/

void FunctionChar (char ch)// ��।������ �㭪樨
{
    std::cout << "�뢮� ���������� ���祭�� � ���� ᨬ����� ᮣ��᭮ ⠡��� ASCII: " << ch << std::endl;
    std::cout << "�뢮� ���������� ���祭�� � 愥���筮� ��⥬� ���᫥��: " << (int) ch <<  std::endl;

}

int main()
{
    std::cout << "������ �� �᫮ �� -128 �� 127: " << std::endl;
    int a; // ������� ��६�����
    std::cin>> a; // ���� 楫��᫥����� ���祭�� � ����������
    char b(a); // ��ॢ�� 楫��᫥����� ���祭�� � ⨯ ������ char, � ᮮ⢥��⢨� � �ॡ�����ﬨ �孨�᪮�� �������
    FunctionChar (b);

    return 0;
}


/*������� �5*/

auto Function_1 (int iValue)-> int // ��।������ �㭪樨
{
    int a = 5; // ��������� ���樠������
    std::cout << "�������� ���祭�� ��६����� ⨯� int  �� -2 147 483 648 �� 2 147 483 647, �.�. ��� ��� �뤥����� 4 ����" << std::endl;
    return 0;
}

auto Function_2 (bool cValue)-> bool // ��।������ �㭪樨
{
    int a (5); // ��ﬠ� ���樠������
    std::cout << "�������� ���祭�� ��६����� ⨯� bool: false = 0, true = �� 1 �� 255, ��� ��� �뤥����� 1 ����" << std::endl;
    return 0;
}

auto Function_3 (char cValue)-> char // ��।������ �㭪樨
{
    int a {5}; // ����� ���樠������
    std::cout << "�������� ���祭�� ��६����� ⨯� bool �� -128 �� 127, �.�. ��� ��� �뤥����� 1 ����" << std::endl;
    return 0;
}

auto Function_4 (long lValue)-> long // ��।������ �㭪樨
{

    std::cout << "�������� ���祭�� ��६����� ⨯� int  �� -2 147 483 648 �� 2 147 483 647, �.�. ��� ��� �뤥����� 4 ����" << std::endl;
    return 0;
}


int main()
{

    Function_1 (1);
    Function_2 (1);
    Function_3 (1);
    Function_4 (1);
    return 0;
}



/*������� �6*/

int a =1; // ���樠������ ������쭮� ��ଥ����

void LocalVariable () // ��।������ �㭪樨
{
    int a = 2; // ���樠������ �����쭮� ��६����� � 䨭�樨 LocalVariable
    std::cout << "�뢮� �����쭮� ��६����� � �� �㭪樨 LocalVariable = " << a << std::endl;

}

int main()
{
    LocalVariable ();
    int a = 3; // ���樠������ �����쭮� ��६����� � 䨭�樨 main
    std::cout << "�뢮� �����쭮� ��६����� � �� �㭪樨 main = " << a << std::endl;
    std::cout << "�뢮� ������쭮� ��६����� � = " << ::a << std::endl;
    return 0;
}




/*������� �7*/

void printChar(int a='a', char b='b', char c ='c', char d='d') // ��।������ �㭪樨
{
    std::cout << "�뢮� ��६����� � � ���� ᨬ���� " << char (a) << std::endl;
    std::cout << "�뢮� ��६����� b � ���� ᨬ���� " << b << std::endl;
    std::cout << "�뢮� ��६����� c � ���� ᨬ���� " << c << std::endl;
    std::cout << "�뢮� ��६����� d � ���� ᨬ���� " << d << std::endl;


    std::cout << "�뢮� ��६����� � � �᫮��� ���� " << a << std::endl;
    std::cout << "�뢮� ��६����� b � �᫮��� ���� " << int (b) << std::endl;
    std::cout << "�뢮� ��६����� c � �᫮��� ���� " << int (c) << std::endl;
    std::cout << "�뢮� ��६����� d � �᫮��� ���� " << int (d) << std::endl;
    std::cout << std::endl;

}

int main()
{
    std::cout << "�맮� �㭪樨 � ��㬥�⠬� �� �笮�砭��: " << std::endl;
    printChar ();
    std::cout << "������ �� ���祭�� ������ ��६����� �� -128 �� 127  " << std::endl;
    int a1;
    std::cin>> a1;
    std::cout << "������ �� ���祭�� ������ ��६����� �� -128 �� 127  " << std::endl;
    int b1;
    std::cin>> b1;
    std::cout << "������ �� ���祭�� ������� ��६����� �� -128 �� 127  " << std::endl;
    int c1;
    std::cin>> c1;
    std::cout << "������ �� ���祭�� ��������� ��६����� �� -128 �� 127  " << std::endl;
    int d1;
    std::cin>> d1;
    std::cout << std::endl;
    std::cout << "�������� �㭪�� � �������묨 ���� 楫��᫥��묨 ���祭�ﬨ:  " << std::endl;
    printChar (a1, b1, c1, d1);

    return 0;
}


/*������� �8*/

int overload (int x, int y); // ����� 楫��᫥����� ⨯�
float overload (float x, float y); // ����� ⨯� � ������饩 ����⮩

int overload(int x, int y)
{
    return x * y;
}
float overload(float x, float y)
{
    return x * y;
}
int main()
{
    int a = 2, b = 5;
    std::cout << " ������� �㭪樨 overload � 楫��᫥��� ⨯�� ��㬥�⮢ (⨯ int) =  " << overload(a, b)  << std::endl;

    float c = 2.5, d = 5.5;
    std::cout << " ������� �㭪樨 overload � ��㬥�⠬� � ������饩 �窮� (⨯ float) = " << overload(c, d) << std::endl;

    return 0; /* �����頥��� ���祭�� */
}


